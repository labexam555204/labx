const express = require("express");

const router = express.Router();
const cryptoJS = require("crypto-js");
const mysql = require("mysql2");
const db = require("../db");
const utils = require("../utils");
const { request } = require("http");
const { response } = require("express");

router.get("/", (request, response) => {
  console.log("hi");
  const query =
    "select movie_id, movie_title, movie_release_date,movie_time,director_name from Movie_Tb";
  db.pool.execute(query, (error, Movie_Tb) => {
    response.send(utils.createResult(error, Movie_Tb));
  });

  router.post("/add", (request, response) => {
    const {
      movie_id,
      movie_title,
      movie_release_date,
      movie_time,
      director_name,
    } = request.body;
    const cryptedPassword = String(cryptoJS.MD5(password));
    const query =
      "insert into Movie_Tb (movie_id, movie_title, movie_release_date,movie_time,director_name) values(?,?,?,?,?)";

    db.pool.execute(
      query,
      [movie_id, movie_title, movie_release_date, movie_time, director_name],
      (error, data) => {
        response.send(utils.createResult(error, data));
      }
    );
  });

  router.get("/get", (request, response) => {
    const { movie_id } = request.body;
    const query =
      "select  movie_title, movie_release_date,movie_time,director_name from Movie_Tb where movie_id=?";
    db.pool.execute(query, [movie_id], (error, Movie_Tb) => {
      if (Movie_Tb.length > 0) {
        response.send(utils.createResult(error, Movie_Tb));
      } else {
        response.send("Movie_Tb not found!!");
      }
    });
  });

  router.delete("/delete", (request, response) => {
    const { movie_id } = request.body;
    const query = "delete from Movie_Tb where movie_id=?";
    db.pool.execute(query, [id], (error, Movie_Tb) => {
      response.send(utils.createResult(error, Movie_Tb));
    });
  });
});
module.exports = router;
